# duerrdental-demo-aws-python

## How to start:

To run this project please install virtual environment dependencies and activate venv.

"chat_client.py" takes the name as a parameter, so please don't forget to provide the name, for example:
```python chat_client.py -=your_name=-```

