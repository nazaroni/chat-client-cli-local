import boto3
from boto3.dynamodb.conditions import Key

def create_counter():
    counter = {
        'id': 1,
        'cValue': 0,
    }
    
    dynamodb = boto3.resource('dynamodb')
    
    table = dynamodb.Table('MessageCounter')
    
    table.put_item(Item=counter)

create_counter()