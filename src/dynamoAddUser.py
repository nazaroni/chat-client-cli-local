import os
import boto3
from boto3.dynamodb.conditions import Key
from dotenv import load_dotenv

# Get environment variables
load_dotenv()

DB_USERS  = os.getenv( 'DB_USER_MESSAGE_HISTORY' )

def db_create_user( name ):
  user = {
    'user_name': name,
    'messages': []
  }
  
  dynamodb = boto3.resource( 'dynamodb' )
  
  table = dynamodb.Table( DB_USERS )
  
  table.put_item( Item=user )