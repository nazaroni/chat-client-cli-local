import os
import boto3
from boto3.dynamodb.conditions import Key
from dotenv import load_dotenv

# Get environment variables
load_dotenv()

DB_USER_MESSAGE_HISTORY    = os.getenv( 'DB_USER_MESSAGE_HISTORY' )

def update_messages_list( user, messages ):
  dynamodb = boto3.resource( 'dynamodb' )
  
  table = dynamodb.Table( DB_USER_MESSAGE_HISTORY )
  
  table.update_item(
    Key={
      'user_name': user,
    },
    UpdateExpression=f"set messages = :g",
    ExpressionAttributeValues={
      ':g': messages
    },
    ReturnValues="UPDATED_NEW"
  )
