import os
import boto3
from boto3.dynamodb.conditions import Key
from dotenv import load_dotenv

# Get environment variables
load_dotenv()

DB_TABLE    = os.getenv( 'DB_TABLE_NAME_MESSAGES' )
DB_COUNTER  = os.getenv( 'DB_MESSAGE_COUNTER_VALUE' )

def update_message_counter( number ):
  dynamodb = boto3.resource( 'dynamodb' )
  
  table = dynamodb.Table( DB_TABLE )
  
  table.update_item(
    Key={
      'id': 1,
    },
    UpdateExpression=f"set { DB_COUNTER } = :g",
    ExpressionAttributeValues={
      ':g': number
    },
    ReturnValues="UPDATED_NEW"
  )
      