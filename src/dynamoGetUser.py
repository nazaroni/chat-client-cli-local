import os
import boto3
from boto3.dynamodb.conditions import Key
from boto3.dynamodb.conditions import Key
from dotenv import load_dotenv

# Get environment variables
load_dotenv()

DB_USERS  = os.getenv( 'DB_USER_MESSAGE_HISTORY' )

def db_get_user( name ):
  dynamodb = boto3.resource( 'dynamodb' )
  
  table = dynamodb.Table( DB_USERS )      
  
  resp = table.get_item(
    Key={
      'user_name' : name,
    }
  )
              
  if 'Item' in resp:
    # print( resp[ 'Item' ] )
    return resp[ 'Item' ]
  else:
    return False

# print( db_get_user( 'John' ) )