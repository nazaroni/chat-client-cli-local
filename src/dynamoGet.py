import boto3
from boto3.dynamodb.conditions import Key

def get_item():
  dynamodb = boto3.resource('dynamodb')
  
  table = dynamodb.Table('MessageCounter')      
  
  resp = table.get_item(
    Key={
      'id' : 1,
    }
  )
              
  if 'Item' in resp:
    return resp[ 'Item' ]
  else:
    pass
