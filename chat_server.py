import os
import socket
import select
import boto3
import json
import moment
from datetime import datetime
from src.dynamoGet import get_item
from src.dynamoUpdate import update_message_counter
from src.dynamoAddUser import db_create_user
from src.dynamoGetUser import db_get_user
from src.dynamoUpdateList import update_messages_list
from dotenv import load_dotenv


# Get environment variables
load_dotenv()

HEADER_LENGTH = os.getenv( 'HEADER_LENGTH' )
IP            = os.getenv( 'IP' )
PORT          = os.getenv( 'PORT' )
DB_COUNTER    = os.getenv( 'DB_MESSAGE_COUNTER_VALUE' )
DB_MESSAGES   = os.getenv( 'DB_USER_MESSAGE_HISTORY' )


# Create a socket
# socket.AF_INET - address family, IPv4, some otehr possible are AF_INET6, AF_BLUETOOTH, AF_UNIX
# socket.SOCK_STREAM - TCP, conection-based, socket.SOCK_DGRAM - UDP,
# connectionless, datagrams, socket.SOCK_RAW - raw IP packets
server_socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

# SO_ - socket option
# SOL_ - socket option level
# Sets REUSEADDR (as a socket option) to 1 on socket
server_socket.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )

# Bind, so server informs operating system that it's going to use given IP and port
# For a server using 0.0.0.0 means to listen on all available interfaces, 
# useful to connect locally to 127.0.0.1 and remotely to LAN interface IP
server_socket.bind(( IP, int( PORT ) ))

# This makes server listen to new connections
server_socket.listen()

# create a client list, list of sockets for select.select()
sockets_list = [ server_socket ]

# List of connected clients - socket as a key, user header and name as data
clients = {}

print( f'Listening for connections on { IP }:{ PORT }...' )

# TODO: create a test for receive_message func
# Handles message receiving
def receive_message( client_msg_socket ):

  try:

    # Receive our 'header' containing message length, it's size is defined and constant
    message_header = client_msg_socket.recv( int( HEADER_LENGTH ) )

    # If we received no data, client gracefully closed a connection, 
    # for example using socket.close() or socket.shutdown(socket.SHUT_RDWR)
    if not len( message_header ):
      return False

    # Convert header to int value
    message_length = int( message_header.decode( 'utf-8' ).strip() )

    # Return an object of message header and message data
    return { 'header': message_header, 'data': client_msg_socket.recv( message_length ) }

  except:
    # If we are here, client closed connection violently, for example by pressing ctrl+c on his script
    # or just lost his connection
    # socket.close() also invokes socket.shutdown(socket.SHUT_RDWR) what sends information about closing the socket (shutdown read/write)
    # and that's also a cause when we receive an empty message
    return False


while True:
  # Calls Unix select() system call or Windows select() WinSock call with three parameters:
  #   - rlist - sockets to be monitored for incoming data
  #   - wlist - sockets for data to be send to (checks if for example buffers are not full and socket is ready to send some data)
  #   - xlist - sockets to be monitored for exceptions (we want to monitor all sockets for errors, so we can use rlist)
  # Returns lists:
  #   - reading - sockets we received some data on (that way we don't have to check sockets manually)
  #   - writing - sockets ready for data to be send thru them
  #   - errors  - sockets with some exceptions
  # This is a blocking call, code execution will 'wait' here and 'get' notified in case any action should be taken
  read_sockets, _, excetions_socket = select.select( sockets_list, [], sockets_list )

  # Iterate over notified sockets
  for notified_socket in read_sockets:

    # If notified socket is a server socket - new connection, accept it
    if notified_socket == server_socket:

      # Accept new connection
      # That gives us new socket - client socket, connected to this given client only, it's unique for that client
      # The other returned object is ip/port set
      client_socket, client_addres = server_socket.accept()

      # Client should send his name right away, receive it
      user = receive_message( client_socket )

      # If False - client disconnected before he sent his name
      if user is False:
        continue

      # Add accepted socket to select.select() list
      sockets_list.append( client_socket )

      # Also save username and username header
      clients[ client_socket ] = user

      print( f'Accepted a new connection from { client_addres[ 0 ] }:\
{ client_addres[ 1 ] } username: { user[ "data" ].decode( "utf-8" ) }' )

      user_name = user[ "data" ].decode( "utf-8" ).strip()

      # Check if user exist in database
      db_user = db_get_user( user_name )

      if ( db_user == False ):
        db_create_user( user_name )
      
    
    # Else existing socket is sending a message
    else:

      # Receive message
      message = receive_message( notified_socket )

      # If False, client disconnected, cleanup
      if message is False:

        print( f'{ clients[ notified_socket ][ "data" ].decode( "utf-8" ) } has left the chat...' )
        
        # Remove from list for socket.socket()
        sockets_list.remove( notified_socket )

        # Remove from our list of users
        del clients[ notified_socket ]
        
        continue

      # Get user by notified socket, so we will know who sent the message
      user = clients[ notified_socket ]

      # Update message counter on the server
      current_message_counter = get_item().get( DB_COUNTER )
      update_message_counter( current_message_counter + 1 )

      print( f'Received message( { current_message_counter + 1 } ) from { user[ "data" ].decode( "utf-8" ) } at { moment.now().format("DD-MM-YYYY H:mm:ss") }: { message[ "data" ].decode( "utf-8" ) }' )
      # print( f'Received message( { current_message_counter + 1 } ) from { user[ "data" ].decode( "utf-8" ) } at { datetime.timestamp( datetime.now() ) }: { message[ "data" ].decode( "utf-8" ) }' )

      # TODO: add this message to DB (max 20)
      user_name = user[ "data" ].decode( "utf-8" ).strip()
      db_user   = db_get_user( user_name )
      user_messages = db_user.get( 'messages' )


      messages_counter = len( db_user.get( 'messages' ) )

      if ( messages_counter < 20 ):
        # TODO: add messages
        user_messages.append( message[ "data" ].decode( "utf-8" ) )
        update_messages_list( user_name, user_messages )
      else:
        # TODO: pop(0) and append new message
        user_messages.pop(0)
        user_messages.append( message[ "data" ].decode( "utf-8" ) )
        update_messages_list( user_name, user_messages )




      # Iterate over connected clients and broadcast message
      for client_socket in clients:

        # But don't sent it to sender
        if client_socket != notified_socket:

          # Send user and message (both with their headers)
          # We are reusing here message header sent by sender, and saved username header send by user when he connected
          client_socket.send( user[ 'header' ] + user[ 'data' ] + message[ 'header' ] + message[ 'data' ] )

  # It's not really necessary to have this, but will handle some socket exceptions just in case
  for notified_socket in excetions_socket:

    # Remove from list for socket.socket()
    sockets_list.remove( notified_socket )

    # Remove from our list of users
    del clients[ notified_socket ]